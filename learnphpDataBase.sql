-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 30, 2023 at 08:13 AM
-- Server version: 8.0.31
-- PHP Version: 8.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `learnphp`
--

-- --------------------------------------------------------

--
-- Table structure for table `pdo`
--

DROP TABLE IF EXISTS `pdo`;
CREATE TABLE IF NOT EXISTS `pdo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `ispublish` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pizzas`
--

DROP TABLE IF EXISTS `pizzas`;
CREATE TABLE IF NOT EXISTS `pizzas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `ingredients` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `pizzas`
--

INSERT INTO `pizzas` (`id`, `name`, `email`, `title`, `ingredients`, `created_at`) VALUES
(1, 'Bolmy Update', 'bolmy09@gmail.com', 'Pizza', 'Tomato', '2023-08-31 16:48:45'),
(2, 'Dane Montgomery', 'xecicusin@mailinator.com', '', 'In accusantium commo', '2023-08-31 18:18:10'),
(3, 'Kards', 'kards09official@gmail.com', '', 'Cheeze', '2023-08-31 18:18:39'),
(6, 'Brooke Floyd', 'puvidyz@mailinator.com', '', 'Labore non ipsum inv', '2023-08-31 18:42:19'),
(7, 'Philip Dalton', 'jawoxyqi@mailinator.com', 'Itaque sed voluptate', 'Veritatis nihil qui ', '2023-08-31 18:49:26'),
(8, 'Macy Mosley', 'logevyd@mailinator.com', 'Aliqua Deleniti lau', 'Ducimus soluta prae', '2023-08-31 18:51:31'),
(10, 'Kiona Santana', 'fila@mailinator.com', 'Eius velit rerum qua', 'In fugiat nihil inci', '2023-08-31 19:00:30'),
(15, 'Pamela Pena', 'sojydu@mailinator.com', 'Nisi sit Nam in ex', 'Aliquam et omnis eaq', '2023-09-01 15:00:24'),
(12, 'Wanda Bean', 'sodyvypad@mailinator.com', 'Non numquam sapiente', 'Iusto laboris saepe ', '2023-08-31 19:07:01'),
(13, 'Barbara Noel', 'cigi@mailinator.com', 'Molestiae dolores vo', 'Exercitationem est t', '2023-08-31 19:07:56'),
(14, 'Nora Wilder', 'sucur@mailinator.com', 'Libero voluptatem p', 'Sunt cillum quis su', '2023-08-31 19:10:54'),
(18, 'Bolmy', 'b@gmail.com', 'Small pizza', 'Tomato and Capcisum', '2023-09-01 15:46:52');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
